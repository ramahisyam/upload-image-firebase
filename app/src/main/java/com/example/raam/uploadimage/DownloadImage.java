package com.example.raam.uploadimage;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class DownloadImage {
    public static void picasso(String url, ImageView imageView)
    {
        Picasso.get().load(url)
                .fit().centerCrop().into(imageView);
    }

}

