package com.example.raam.uploadimage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailActivity extends AppCompatActivity {

    private TextView mDesc, mTitle;
    private ImageView mImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = findViewById(R.id.title);
        mDesc = findViewById(R.id.desc);
        mImg = findViewById(R.id.img);

        Bundle extras = getIntent().getExtras();

        mDesc.setText(extras.getString("desc"));
        mTitle.setText(extras.getString("title"));

        Glide.with(this).load(extras.getString("img")).into(mImg);

    }

}
