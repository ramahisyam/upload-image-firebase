package com.example.raam.uploadimage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class DataAdapter  extends RecyclerView.Adapter<DataAdapter.DataViewHolder> {

    private Context mContext;
    private List<Model> mModel;
    private RecyclerViewClickListener mListener;

    public DataAdapter(Context mContext, List<Model> mModel) {
        this.mContext = mContext;
        this.mModel = mModel;
    }

    public DataAdapter(RecyclerViewClickListener mListener) {
        this.mListener = mListener;
    }

    public DataAdapter(Context mContext, List<Model> mModel, RecyclerViewClickListener mListener) {
        this.mContext = mContext;
        this.mModel = mModel;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.data_item, viewGroup, false);
        return new DataViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder dataViewHolder, int i) {
        Model model = mModel.get(i);
        Glide.with(mContext).load(model.getImage()).into(dataViewHolder.dataImage);
        dataViewHolder.titleContent.setText(model.getJudul());
    }

    @Override
    public int getItemCount() {
        return mModel.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView dataImage;
        TextView dataContent, titleContent;

        private RecyclerViewClickListener mListener;


        @Override
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition());
        }

        public DataViewHolder(@NonNull View itemView, RecyclerViewClickListener listener) {
            super(itemView);

            mListener = listener;
            itemView.setOnClickListener(this);
            dataImage = itemView.findViewById(R.id.data_image);
            titleContent = itemView.findViewById(R.id.title_content);
        }
    }

    public interface RecyclerViewClickListener {

        void onClick(View view, int position);
    }
}
