package com.example.raam.uploadimage;

public class Model {
    private String image;
    private String judul;
    private String descImage;

    public Model(String image, String judul, String descImage) {
        this.image = image;
        this.judul = judul;
        this.descImage = descImage;
    }

    public Model() {
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescImage() {
        return descImage;
    }

    public void setDescImage(String descImage) {
        this.descImage = descImage;
    }
}
